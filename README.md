<h1>Sentiment Analysis</h1>

The purpose of this project is to learn PyTorch and TensorFlow 2.0 while conducting a research in Sentiment Analysis.
More notebooks/projects with different architectures, datasets, and frameworks will be added in the future.

## Directory
1.  **Model_Evaluatons** - contains different applications of NLP with each application being evaluated by different Models/Algorithms
    *  `Models.ipynb` - comparing `Multinomial Naive Bayes`, `SVM`, and `Bidirectional LSTM` on a small dataset
    *  `w2v_ft.ipynb` - `Word2Vec` and `FastText` word vector implementations for the embeddings
2.  **PyTorch** - projects implemented in PyTorch for practicing
3.  **Data_Cleaning** - contains notebooks or scripts for cleaning and preprocessing data

## Datasets
1.  IMDB Dataset - http://ai.stanford.edu/~amaas/data/sentiment/
2.  FB sentiments Dataset - cleaned_dataset.csv

## References
*  https://github.com/bentrevett/pytorch-sentiment-analysis